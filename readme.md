## 使用说明(本地python版本要求3.7.5或以上)
### 环境安装包依赖
##### 1，安装utx模块
`cd utx-master/` 
`python setup.py install`
##### 2，安装读取ini文件的包
`pip install configobj`
##### 3，安装selenium
`pip install selenium`
##### 4，安装模拟键盘控制的包
###### 设置程序可操作键盘 System Preferences/Security&Privacy/Privacy
`pip install pyautogui`


### 目录结构说明
##### 1，common包
###### case_read:读取config目录下的data_model.csv中的页面元素信息
###### config_set:读或写config目录下的config.ini中的配置信息
###### HTMLTESTRunner_Char:生成最近10次测试报告的模版(模板来源：https://github.com/githublitao/HTMLTestRunner_Chart)
###### send_mail:测试执行完毕后发送电子邮件
###### webUI:封装常用的页面元素操作方法及对获取的测试元素进行解析运行

##### 2，config
###### chromedriver：谷歌浏览器驱动
###### config.ini:环境变量信息或者临时变量信息存放文件
###### data_model.csv：页面元素存放文件

##### 3，report
###### 存放测试报告（HTML文件：可查看详细测试用例内容；JSON文件用来存储最近10次运行的测试结果）

##### 4，stratifydObject
###### 公用的测试用例或公用页面抽离

##### 5，stratifydTC
###### 测试用例文件夹

##### 6，utx-master（utx源码参考：https://github.com/jianbing/utx）
###### 功能：
######1、让测试用例可按照编写顺序执行
######2、可对测试用例进行标记（标记版本号及冒烟测试用例，以区别运行不同的测试用例）
######3、可以增加数据驱动功能

##### 7，其它
###### run_chart.py:运行全部测试用例；run_chart_smoke.py:运行冒烟测试用例

