import time

from utx import *
import logging
import os
import unittest

from common import get_version
from common.ex_tool import  send_file_http
from common.send_mail import send_mail
from common.HTMLTestRunner_en import HTMLTestRunner
cur_path = os.path.dirname(os.path.realpath(__file__))


report_time=time.strftime("%Y%m%d%H%M", time.localtime())
case_path = os.path.join(cur_path, "stratifydTC")
report_path=os.path.join(cur_path,"report/stratifydUI_"+report_time+".html")


if __name__ == '__main__':

    # 获取前端版本号：
    frontend_version = get_version.get_frontend_version()

    setting.run_case = {Tag.ALL}  # 运行全部测试用例
    # setting.run_case = {Tag.SMOKE}  # 只运行SMOKE标记的测试用例
    # setting.run_case = {Tag.SMOKE, Tag.V1_0_0}   # 只运行SMOKE和V1_0_0标记的测试用例

    setting.check_case_doc = False  # 关闭检测是否编写了测试用例描述
    setting.full_case_name = True
    setting.max_case_name_len = 80  # 测试报告内，显示用例名字的最大程度
    setting.show_error_traceback = True  # 执行用例的时候，显示报错信息
    setting.sort_case = True  # 是否按照编写顺序，对用例进行排序
    setting.show_print_in_console = True

    log.set_level(logging.DEBUG)  # 设置utx的log级别
    # log.set_level_to_debug()     # 设置log级别的另外一种方法
    # 运行测试用例并生成报告
    suite = unittest.defaultTestLoader.discover(case_path,pattern='*.py',top_level_dir=None)
    runner = HTMLTestRunner(
        title="stratifyd_test_report",
        description="stratifyd BJ TestReport <br>"
                    "frontend version:{0} <br>"
                    "backend version:".format(frontend_version),
        stream=open(report_path, "wb"),
        verbosity=2,
        retry=1,
        save_last_try=True)
    runner.run(suite)
    # 生成测试报告后发送邮件

    # 将文件传到能查看报告的aws服务器上
    tag_path = "/zhuhaihua/django_report/stratifyd_tp/templates/" + "stratifydUI_" + report_time + ".html"
    time.sleep(10)
    print(report_path)
    print(tag_path)
    # send_file_http(os.path.join(cur_path, "report/stratifyd_testReport.json"))
    send_file_http(os.path.join(cur_path, report_path))
    # send_file(report_path, tag_path)

    # send_mail("斯图飞腾-自动化测试报告", "http://54.201.226.154:8000/STReport/ui/stratifydUI_"+report_time+".html")
send_mail("Stratifyd UI Test Report", "http://54.201.226.154:8000/STReport/ui/stratifydUI_"+report_time+".html")
# send_mail("斯图飞腾-自动化测试报告", "http://54.201.226.154:8000/STReport/ui/stratifyd_testReport.html")


