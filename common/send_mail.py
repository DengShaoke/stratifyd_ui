import os
import time
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
import smtplib
from time import sleep

from common.config_set import get_var
from common.webUI import WebUI

cur_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.dirname(cur_path)
pic_name="test_result.png"


def send_mail(subject, report_url):
    test_report_pic = WebUI()
    test_report_pic.web_open(report_url)
    sleep(3)
    aaa = test_report_pic.get_png()
    test_report_pic.web_quit()

    test_finished_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    email_host = 'smtp.stratifyd.cn'  # 服务器地址
    sender = 'shaoke.deng@stratifyd.cn'  # 发件人（自己的邮箱）
    password = 'Deng0727'  # 密码（自己邮箱的登录密码）
    receivers = 'ming@stratifyd.cn,chao.wang@stratifyd.cn,haihua.zhu@stratifyd.cn,shaoke.deng@stratifyd.cn'
    receiver = receivers.split(",")  # 收件人

    msg = MIMEMultipart()
    msg['Subject'] = subject  # 标题
    msg['From'] = "shaoke.deng@stratifyd.cn"  # 邮件中显示的发件人别称
    msg['To'] = receivers  # ...收件人...

    signature = '''
\n\t this is auto test report!
\n\t run by BJ tester..
'''
    mail_msg = '''
<p>\n\t this is auto test report，test finished time:{0}</p>
<p>\n\t check detail info click below link</p>
<a href="{1}">TestReport</a>
<p><img src="cid:image1"></p>
'''.format(test_finished_time, report_url)
    msg.attach(MIMEText(mail_msg, 'html', 'utf-8'))
    msgImage = MIMEImage(aaa)
    msgImage.add_header('Content-ID', '<image1>')
    msg.attach(msgImage)

    ctype = 'application/octet-stream'
    maintype, subtype = ctype.split('/', 1)
    # 附件-图片
    image = MIMEImage(aaa, _subtype=subtype)

    # image = MIMEImage(open(result_pic, 'rb').read(), _subtype=subtype)
    image.add_header('Content-Disposition', 'attachment', filename=pic_name)
    msg.attach(image)

    # 发送
    s = smtplib.SMTP_SSL(email_host, 465)
    s.login(sender, password)
    s.sendmail(sender, receiver, msg.as_string())
    s.quit()

if __name__ == '__main__':
    send_mail("Stratfifyd Test Report")