import os

from configobj import ConfigObj

cur_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.dirname(cur_path)
config_init_path = os.path.join(parent_path, "config/config.ini")

cf = ConfigObj(config_init_path)


def get_var(session, key):
    var = cf[session][key]
    return var


def set_var(session, key, value):
    cf[session][key] = value
    cf.write()


if __name__ == '__main__':
    print(config_init_path)
    a = get_var("env", "url")
    print(a)
