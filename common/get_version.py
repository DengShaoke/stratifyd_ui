import time

from common.config_set import get_var
from common.webUI import WebUI
from selenium.webdriver.common.action_chains import ActionChains


def get_frontend_version():
    frontend_version = "Not get the frontend version"
    try:
        ui = WebUI()
        ui.web_open(get_var("env", "uat_url"))
        time.sleep(5)
        ele_ui_version = ui.driver.find_element_by_css_selector(".brand-logo")
        ActionChains(ui.driver).move_to_element(ele_ui_version).perform()
        frontend_version = ele_ui_version.text
    except Exception as e:
        print(e)
        print("can not get the frontend version")
    finally:
        ui.web_quit()
    return frontend_version


if __name__ == '__main__':
    a=get_frontend_version()
    print(a)
    print(type(a))

