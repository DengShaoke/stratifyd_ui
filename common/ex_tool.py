import os
import platform
import sys
import time

import paramiko
import requests
from openpyxl import load_workbook

cur_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.dirname(cur_path)
download_path = os.path.join(parent_path, "test_result")  # 下载路径


def time_now():
    return time.strftime("%Y%m%d%H%M%S", time.localtime())


def get_chorome_driver():
    # http://npm.taobao.org/mirrors/chromedriver/ 版本地址
    local_system = platform.system()
    if local_system == "Windows":
        driver_path = os.path.join(parent_path, "config/chromedriver_windows.exe")  # driver路径
    if local_system == "Linux":
        driver_path = os.path.join(parent_path, "config/chromedriver_linux")  # driver路径
    if local_system == "Darwin":
        driver_path = os.path.join(parent_path, "config/chromedriver")  # driver路径
    return driver_path


def send_file_ssh(src, tag):
    number = 0
    send = False
    while number < 10 and not send:
        try:
            print("strart")
            transport = paramiko.Transport(('54.201.226.154'))
            transport.connect(username='zhuhaihua', password='paC51B-vj')
            sftp = paramiko.SFTPClient.from_transport(transport)
            print("strp:", sftp)
            a = sftp.put(src, tag, confirm=True)
            print("a:", a)
            transport.close()
            print("finished")
            send = True
        except Exception as e:
            send = False
            print("传失败", number + 1, "次")
            print(src)
            print(tag)
        finally:
            number = number + 1


def send_file_http(file_path):
    number = 0
    send = False
    while number < 10 and not send:
        try:
            url = "http://54.201.226.154:8000/STReport/upload/"
            file = {
                'send_file': open(file_path, 'rb')
            }
            data = {"tag": "ui"}
            response = requests.post(url, data=data, files=file)
            if response.status_code == 200 and response.text == "finished":
                send = True
        except Exception as e:
            print("传失败", number + 1, "次")
            print(e)
        finally:
            number = number + 1
            print(response.status_code)
            print(response.text)


def get_excel_row_number(excel_path):
    wb = load_workbook(filename=excel_path)
    sheet = wb["Sheet 1"]
    return sheet.max_row

def get_csv_row_number(csv_path):
    with open(csv_path, "r", encoding="utf-8") as f:
        return len(f.readlines())


if __name__ == '__main__':
    get_csv_row_number("/Users/kedeng/Downloads/22.csv")
