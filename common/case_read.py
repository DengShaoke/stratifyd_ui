import csv
import os

cur_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.dirname(cur_path)
csv_path = os.path.join(parent_path, "user_data/stratifyd_page.csv")


# 获取csv中数据模版
def read_test_csv(page, ele_no):
    with open(csv_path, 'r', encoding="utf-8") as f:
        csv_file = csv.DictReader(f)
        for row in csv_file:
            if row.get("page") == page and row.get("ele_no") == ele_no:
                return row
        else:
            print("can not get the ")
            return None
# 读预期结果
def read_except_csv(page, ele_no):
    with open(csv_path, 'r') as f:
        csv_file = csv.DictReader(f)
        for row in csv_file:
            if row.get("page") == page and row.get("ele_no") == ele_no:
                return row
        else:
            print("can not get the ")
            return None


if __name__ == '__main__':
    a=read_test_csv("advnaced_stopwords", "12")
    print(a.get('ele').format(2, 2))
