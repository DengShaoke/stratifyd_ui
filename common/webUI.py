import os
import time
from time import sleep

from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
# from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait

from common.case_read import read_test_csv
from common.config_set import get_var
from common.ex_tool import get_chorome_driver
cur_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.dirname(cur_path)
download_path = os.path.join(parent_path, "test_result")  # 下载路径

def f_time(f):
    def inner(self,*args, **kw):
        start_time = time.time()
        f(self, *args, **kw)
        end_time = time.time()
        print("运行花费时间：", end_time-start_time, "(s)")
    return inner

class WebUI(object):
    def __init__(self):
        options = webdriver.ChromeOptions()
        if get_var('env', "headless") == "yes":
            options.add_argument('headless')  # 静默模式
        prefs = {'profile.default_content_settings.popups': 0, 'download.default_directory': download_path}
        options.add_argument('--unsafely-treat-insecure-origin-as-secure=http://192.168.3.122:2012')
        options.add_experimental_option('prefs', prefs)
        self.driver = webdriver.Chrome(executable_path=get_chorome_driver(), chrome_options=options)
        # self.driver.set_window_size(1920, 1080)  # 可根据需要设置分辨率
        self.driver.maximize_window()  # 打开的浏览器driver驱动设置
        print(self.driver.get_window_size())
        # 设置浏览器非安全模式下也能运行


    def web_open(self, url):
        print("navigated to url", url)
        self.driver.get(url)
        time.sleep(2)

    def web_refresh(self):
        self.driver.refresh()

    def web_quit(self):
        self.driver.quit()

    def web_close(self):
        self.driver.close()

    def if_element_isExist(self, by_type,by_element):
        try:
            self.find_by(by_type, by_element)
            flag = True
        except Exception as e:
            flag = False
        return flag

    def find_by(self, by_type=None, by_element=None):
        STYLE = "background: green; border: 2px solid red;"
        # 先判断页面是不是加载完了，国外服务器也太慢了吧。。。。。
        STR_READY_STATE = ''
        time_start = time.time()
        while STR_READY_STATE != 'complete':
            time.sleep(0.01)
            STR_READY_STATE = self.driver.execute_script('return document.readyState')
            time_end = time.time()
            if int(time_end - time_start) > 300:
                assert False, "more than 5 minutes, the page is not load complete!"
        ele = None
        by_type = by_type.lower()
        wait = WebDriverWait(self.driver, 60)
        try:
            assert by_type, "check your by type:id, name, css, xpath"
            if by_type == "id":
                ele = wait.until(lambda x: x.find_element_by_id(by_element))
            if by_type == "name":
                ele = wait.until(lambda x: x.find_element_by_name(by_element))
            if by_type == "css":
                ele = wait.until(lambda x: x.find_element_by_css_selector(by_element))
            if by_type == "xpath":
                ele = wait.until(lambda x: x.find_element_by_xpath(by_element))
            # self.driver.execute_script("arguments[0].setAttribute('style', arguments[1]);", ele, STYLE)
        except Exception as e:
            print({"by_type": by_type, "by_element": by_element})
            assert False, e
        finally:
            return ele

    def handle_ele(self, handle_type=None, by_type=None, by_element=None, value=None):
        sleep(0.5)
        handle_type_list = ["chk_text", "submit", "click", "send_keys", "chk_value", "chk_select", "select"]
        handle_type = handle_type.lower()
        handle = self.find_by(by_type, by_element)
        if handle_type == "chk_value":
            print("verified page contains text:", "[", value, "]")
            get_value = handle.get_attribute("value")
            if not get_value.find(value) + 1:
                print("预期结果：" + value)
                print("实际结果：" + get_value)
                assert False
            print("预期结果：" + value)
            print("实际结果：" + get_value)

        if handle_type == "chk_select":
            print("verified the default select is:", "[", value, "]")
            get_value = Select(handle).first_selected_option.text
            if not get_value.find(value) + 1:
                print("预期结果：" + value)
                print("实际结果：" + get_value)
                assert False
            print("预期结果：" + value)
            print("实际结果：" + get_value)
        if handle_type == "chk_text":
            print("verified page contains text:", "[", value, "]")
            get_text = handle.text
            time_start = time.time()
            while not get_text.find(value) + 1:
                time.sleep(0.5)
                get_text = handle.text
                time_end = time.time()
                # print(get_text)
                if int(time_end - time_start) > 60:
                    print("预期结果：" + value)
                    print("实际结果：" + get_text)
                    assert False, "more than  60 seconds can not get the right text"
            print("预期结果：" + value)
            print("实际结果：" + get_text)
        if handle_type == "chk_long":
            print("verified page contains text:", "[", value, "]")
            get_text = handle.text
            time_start = time.time()
            while not get_text.find(value) + 1:
                time.sleep(0.5)
                get_text = handle.text
                time_end = time.time()
                # print(get_text)
                if int(time_end - time_start) > 300:
                    print("预期结果：" + value)
                    print("实际结果：" + get_text)
                    assert False, "more than  300 seconds can not get the right text"
            print("预期结果：" + value)
            print("实际结果：" + get_text)
        else:
            try:
                assert handle_type in handle_type_list, "check your handle_type:chk_text, submit, click, send_keys"
                if handle_type == "submit":
                    handle.submit()
                if handle_type == "click":
                    wait = WebDriverWait(self.driver, 60)
                    wait.until(EC.element_to_be_clickable, handle)
                    try:
                        handle.click()
                    except Exception  as e:
                        self.driver.execute_script("(arguments[0]).click()", handle)

                if handle_type == "send_keys":
                    handle.clear()
                    sleep(0.5)
                    if handle.get_attribute("value") != "":
                        self.driver.execute_script("(arguments[0]).value=''", handle)
                    handle.clear()
                    sleep(0.5)
                    handle.send_keys(value)
                if handle_type == "select":
                    Select(handle).select_by_index(int(value))
                    sleep(0.05)
            except Exception as e:
                print({"handle_type": handle_type, "by_type": by_type, "by_element": by_element, "value": value})
                assert False, e
            time.sleep(1)
    @f_time
    def ui_run(self, page, ele_no, value=None):
        run_data = read_test_csv(page, ele_no)
        print(page, ele_no, run_data.get("remarks"))

        if value is None:
            value = run_data.get("value")
        if value == "lang-zh":
            value = run_data.get('lang-zh')
        if value == 'lang-en':
            value = run_data.get('lang-en')
        print(run_data.get("ele_description"), ": [", value, "] <<", run_data.get("ele_by_type"), ":",
              run_data.get("ele"), ">>")
        self.handle_ele(run_data.get("ele_handle_type"), run_data.get("ele_by_type"), run_data.get("ele"), value)

    def get_img(self):
        return self.driver.get_screenshot_as_base64()

    def save_img(self, result_pic):
        self.driver.get_screenshot_as_file(result_pic)

    def get_png(self):
        return self.driver.get_screenshot_as_png()


if __name__ == '__main__':
    d = WebUI()
    d.web_open("https://uat.stratifyd.com/login.html")

    # d.handle_ele("send_keys", "css", "input[name='user']", "shaoke.deng@stratifyd.cn")
    # d.handle_ele("send_keys", "css", "input[name='password']", "Deng0727@stratifyd")
    # d.handle_ele("submit", "css", ".login button")
    # d.web_quit()
