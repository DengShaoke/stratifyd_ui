import os
from time import sleep

from common.case_read import read_test_csv
from common.config_set import get_var
from common.ex_tool import time_now
from common.webUI import WebUI

cur_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.dirname(cur_path)
class stratifyd(WebUI):
    def __init__(self):
        super().__init__()
        self.lang = 'lang-en'

    def backend_subdomain(self):
        self.ui_run("uatsubdomain", "1")
        self.ui_run("uatsubdomain", "3", get_var("env", "backend"))
        self.ui_run("uatsubdomain", "2", get_var("env", "subdomain"))
        sleep(1)
        self.ui_run("uatsubdomain", "4")
        sleep(1)

    def login(self, user=None, pwd=None):
        self.ui_run("login", "1", user)
        self.ui_run("login", "2", pwd)
        sleep(0.5)
        self.ui_run("login", "3")
        self.lang = "lang-en"
        print(self.lang)

    def logout(self):
        self.ui_run("home_top", "1")
        self.ui_run("home_top", "3")

    def login_uat(self):
        self.web_open(get_var("env", "uat_url"))
        self.backend_subdomain()
        self.login(get_var("data", "user"), get_var("data", "pwd"))
        self.ui_run("home_top", "2", self.lang)  # 验证是否登录成功

    def create_stopwords(self):
        run_data = read_test_csv("Advanced_New_Custom_Stopwords_List", "11")
        stopwords = ["hello", "你猜", "不想说"]
        i = 1
        for stopword in stopwords:
            self.ui_run("Advanced_New_Custom_Stopwords_List", "10")
            # sleep(1)
            print("Enter term:", stopword)
            self.handle_ele(run_data.get("ele_handle_type"),
                            run_data.get("ele_by_type"),
                            run_data.get("ele").format(i),
                            stopword)
            i = i + 1

    def create_sentiment(self):
        run_data = read_test_csv("Advanced_New_Custom_Sentiment_List", "11")
        sentiment = [{"term": "happy", "sentiment": "5", "Description": "very happy"},
                     {"term": "angry", "sentiment": "-5", "Description": "very angry"},
                     {"term": "en", "sentiment": "0", "Description": "nothing"},
                     ]
        i = 1
        for s in sentiment:
            self.ui_run("Advanced_New_Custom_Sentiment_List", "10")
            # sleep(1)
            print("Enter term:", s.get("term"))
            self.handle_ele(run_data.get("ele_handle_type"),
                            run_data.get("ele_by_type"),
                            run_data.get("ele").format(i, 1),
                            s.get("term"))
            print("Enter sentiment:", s.get("sentiment"))
            # sleep(0.5)
            self.handle_ele(run_data.get("ele_handle_type"),
                            run_data.get("ele_by_type"),
                            run_data.get("ele").format(i, 2),
                            s.get("sentiment"))
            # sleep(0.5)
            print("Enter Description:", s.get("Description"))
            self.handle_ele(run_data.get("ele_handle_type"),
                            run_data.get("ele_by_type"),
                            run_data.get("ele").format(i, 3),
                            s.get("Description"))
            i = i + 1

    def dashborad_delete_fileds(self):
        print("删除默认的两个fileds")
        for i in range(2):
            have_filed = self.find_by("css", ".fa-times-circle")
            sleep(1)
            have_filed.click()

    def check_filter_count(self):
        count = self.driver.find_elements_by_css_selector(
            ".react-grid-layout>.react-draggable:nth-child(2) .etable-body>div.etable-row")
        return len(count)

    def check_model_count(self):
        count = self.driver.find_elements_by_css_selector("table>tbody>tr:last-child>td>dl>dd")
        return len(count)

    def remove_bottom_filter(self):
        print("check if bottom filter exist, remove it")
        bottom_filter_exist = False
        try:
            bottom_filters = self.driver.find_elements_by_css_selector(".content .expr")
            bottom_filter_exist = True
        except Exception as e:
            print(e)
            print("not found the filters on bottom")
        if bottom_filter_exist:
            print("found ", len(bottom_filters), " bottom filters")
            for i in range(len(bottom_filters)):
                try:
                    bottom_filter = self.driver.find_element_by_css_selector(".content .expr")
                    bottom_filter.click()
                    sleep(1)
                    filter_close = self.driver.find_element_by_css_selector(".filter-popover .sub-group .title>a>i")
                    filter_close.click()
                    sleep(1)
                    try:
                        btn = self.driver.find_element_by_css_selector(".btn-success")
                        btn.click()
                        print("remove tab filter")
                        sleep(1)
                    except Exception as e:
                        print("remove global filter")
                except Exception as e:
                    print(e)
                    print("remove bottom_filters faild")
                finally:
                    sleep(3)
            print("remove widget filters")
            try:
                self.ui_run("Dashboards_Overview_wiget_filter", "1")
                sleep(1)
                self.ui_run("Dashboards_Overview_wiget_filter", "2")
                sleep(1)
                self.ui_run("Dashboards_Overview_wiget_filter", "9")
                sleep(1)
                self.ui_run("Dashboards_Overview_wiget_filter", "8")
            except Exception as e:
                self.ui_run("Dashboards_Overview_wiget_filter", "12")
                print(" not found widget")



    def overview_open(self, btn): # 在dashboard中的概览中打开筛选Filters/数据Data/警报Alerts/校准数据Tune
        if btn == "Filters":
            print("on page Dashboard_overview, click Btn:Filters")
            Filters = self.find_by("css", ".comparison-view-side-toolbar> .toggle-list:nth-child(1)>li:nth-child(1)")
            Filter = Filters.get_attribute("class")
            if Filter != "active":
                Filters.click()
        if btn == "Data":
            print("on page Dashboard_overview, click Btn:Data")
            Filters = self.find_by("css", ".comparison-view-side-toolbar> .toggle-list:nth-child(1)>li:nth-child(2)")
            Filter = Filters.get_attribute("class")
            print(Filter)
            if Filter != "active":
                Filters.click()
        if btn == "Alerts":
            print("on page Dashboard_overview, click Btn:Alerts")
            Filters = self.find_by("css", ".comparison-view-side-toolbar> .toggle-list:nth-child(1)>li:nth-child(3)")
            Filter = Filters.get_attribute("class")
            if Filter != "active":
                Filters.click()
        if btn == "Tune":
            print("on page Dashboard_overview, click Btn:Tune")
            Filters = self.find_by("css", ".comparison-view-side-toolbar> .toggle-list:nth-child(2)>li:nth-child(1)")
            Filter = Filters.get_attribute("class")
            if Filter != "active":
                Filters.click()

    def remove_the_second_widget(self):
        try:
            ele = self.find_by("css", ".react-grid-layout>.react-draggable:nth-child(2) .option-toggle")
        except Exception as e:
            print("no widget need remove")
            ele = False
        if ele:
            self.ui_run("Dashboards_wiget_delete", "1")
            sleep(2)
            self.ui_run("Dashboards_wiget_delete", "2")

    def create_dataStreams(self):
        '''CSV File Ingest'''
        try:
            csv_file = os.path.join(parent_path, "user_data/sample_data_1/hh_taxonomy.csv")
            # time.sleep(3)
            print(csv_file)
            self.ui_run("HomePage_Data_Streams", "1")
            # time.sleep(1)
            self.ui_run("common_btn_create", "1")
            self.ui_run("Data_Streams_New_Data", "2")
            self.ui_run("Data_Streams_Connect_to_the_Data_Stream", "1_2", self.lang)
            csv_streams_name = "csv_data" + time_now()
            self.ui_run("Data_Streams_Connect_to_the_Data_Stream", "2", csv_streams_name)
            # time.sleep(3)
            upload = self.find_by("css", ".dnd-dropzone input")
            self.driver.execute_script(
                'arguments[0].style = ""; arguments[0].style.display = "block"; arguments[0].style.visibility = "visible";',
                upload)
            upload.send_keys(csv_file)
            sleep(2)
            # self.ui_run("Data_Streams_Connect_to_the_Data_Stream", "4_2", self.lang)
            if self.lang=="lang-en":
                self.ui_run("Data_Streams_Connect_to_the_Data_Stream", "4_2",
                               "1 file(s) ingested, 1,096 records ready for analysis")
            if self.lang=="lang-zh":
                self.ui_run("Data_Streams_Connect_to_the_Data_Stream", "4_2",
                               "1 文件已完成录入, 1,096条数据可用于分析任务")
            self.ui_run("Data_Streams_Connect_to_the_Data_Stream", "5")
            sleep(2)
            self.ui_run("Data_Streams_Connect_to_the_Data_Stream", "6", csv_streams_name)
        except Exception as e:
            print("Error: 数据流创建失败")
            assert False, e
        return csv_streams_name



if __name__ == '__main__':
    a = stratifyd()
    a.login_uat()
