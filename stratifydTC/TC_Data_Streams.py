import unittest
from common.ex_tool import *
from common.config_set import get_var
from stratifydObject.stratifydUI import stratifyd

cur_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.dirname(cur_path)


class TC_UAT(unittest.TestCase):
    st = stratifyd()

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        cls.st.web_quit()

    def add_img(self):
        self.imgs.append(self.st.get_img())
        return True

    def setUp(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        # self.driver = webdriver.Chrome()
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    def tearDown(self):
        self.st.web_refresh()

    # @unittest.skip("已调试通过，跳过")
    def test_excel_file_ingest(self):
        '''Excel File Ingest'''
        try:
            excel_file = os.path.join(parent_path, get_var("env", "excel_file"))
            print(excel_file)
            self.st.login_uat()
            # time.sleep(3)
            self.st.ui_run("HomePage_Data_Streams", "1")
            # time.sleep(1)
            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Data_Streams_New_Data", "1")
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "1_1", self.st.lang)
            excel_streams_name = "excel_data" + time_now()
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "2", excel_streams_name)
            # time.sleep(3)
            upload = self.st.find_by("css", ".dnd-dropzone input")
            self.st.driver.execute_script(
                'arguments[0].style = ""; arguments[0].style.display = "block"; arguments[0].style.visibility = "visible";',
                upload)
            upload.send_keys(excel_file)
            time.sleep(2)
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "4_1", self.st.lang)
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "5")
            time.sleep(2)
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "6", excel_streams_name)
        except Exception as e:
            self.add_img()
            assert False, e

    # @unittest.skip("已调试通过，跳过")
    def test_csv_file_ingest(self):
        '''CSV File Ingest'''
        try:
            csv_file = os.path.join(parent_path, get_var("env", "csv_file"))
            print(csv_file)
            self.st.login_uat()
            # time.sleep(3)
            self.st.ui_run("HomePage_Data_Streams", "1")
            # time.sleep(1)
            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Data_Streams_New_Data", "2")
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "1_2", self.st.lang)
            csv_streams_name = "csv_data" + time_now()
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "2", csv_streams_name)
            # time.sleep(3)
            upload = self.st.find_by("css", ".dnd-dropzone input")
            self.st.driver.execute_script(
                'arguments[0].style = ""; arguments[0].style.display = "block"; arguments[0].style.visibility = "visible";',
                upload)
            upload.send_keys(csv_file)
            time.sleep(2)
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "4_2", self.st.lang)
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "5")
            time.sleep(2)
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "6", csv_streams_name)
        except Exception as e:
            self.add_img()
            assert False, e

    # @unittest.skip("已调试通过，跳过")
    def test_json_file_ingest(self):
        '''JSON File Ingest'''
        try:
            json_file = os.path.join(parent_path, get_var("env", "json_file"))
            print(json_file)
            self.st.login_uat()
            # time.sleep(3)
            self.st.ui_run("HomePage_Data_Streams", "1")
            # time.sleep(1)
            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Data_Streams_New_Data", "3")
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "1_3", self.st.lang)
            json_streams_name = "json_data" + time_now()
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "2", json_streams_name)
            # time.sleep(3)
            upload = self.st.find_by("css", ".dnd-dropzone input")
            self.st.driver.execute_script(
                'arguments[0].style = ""; arguments[0].style.display = "block"; arguments[0].style.visibility = "visible";',
                upload)
            upload.send_keys(json_file)
            time.sleep(2)
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "4_3", self.st.lang)
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "5")
            time.sleep(2)
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "6", json_streams_name)
        except Exception as e:
            self.add_img()
            assert False, e


if __name__ == '__main__':
    unittest.main()
