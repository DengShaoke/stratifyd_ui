import unittest
from common.ex_tool import *
from common.config_set import get_var
from stratifydObject.stratifydUI import stratifyd

cur_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.dirname(cur_path)


class TC_UAT(unittest.TestCase):
    st = stratifyd()

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        cls.st.web_quit()

    def add_img(self):
        self.imgs.append(self.st.get_img())
        return True

    def setUp(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        # self.driver = webdriver.Chrome()
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    def tearDown(self):
        self.st.web_refresh()

    # @unittest.skip("已调试通过，跳过")
    def test_deploy_txonomy_one_text(self):
        try:
            self.st.login_uat()
            # time.sleep(1)
            self.st.ui_run("HomePage_Dashboards", "1")

            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Dashboards_New_Dashboard", "0")
            dashboard_tx_name = "deploy_txonomy1T" + time_now()
            self.st.ui_run("Dashboards_New_Dashboard", "1", dashboard_tx_name)
            self.st.ui_run("Dashboards_New_Dashboard", "2")
            self.st.ui_run("Dashboards_New_Dashboard", "3")
            time.sleep(3)
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "1")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "2")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "3", self.st.lang)
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "1")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_1")
            time.sleep(5)
            print("Taxonomy,选择数据-text格式")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "5", self.st.lang)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "6")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "1")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "2")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "3")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "4")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "5")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "6")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "7")
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "3")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Overview", "1")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            # time.sleep(5)
        except Exception as e:
            self.add_img()
            assert False, e

    # @unittest.skip("已调试通过，跳过")
    def test_deploy_txonomy_two_text(self):
        try:
            self.st.login_uat()
            # time.sleep(1)
            self.st.ui_run("HomePage_Dashboards", "1")
            # time.sleep(2)
            self.st.ui_run("common_btn_create", "1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_New_Dashboard", "0")
            dashboard_tx_name = "deploy_txonomy2T" + time_now()
            # dashboard_tx_name = "DK99TEST"
            self.st.ui_run("Dashboards_New_Dashboard", "1", dashboard_tx_name)
            self.st.ui_run("Dashboards_New_Dashboard", "2")
            self.st.ui_run("Dashboards_New_Dashboard", "3")
            time.sleep(3)
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "1")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "2")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "3", self.st.lang)
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "1")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_1")
            time.sleep(5)
            print("Taxonomy,选择数据-text格式")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            # time.sleep(1)
            print("Taxonomy,选择数据-text格式")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1_1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "5", self.st.lang)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "6")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "1")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "2")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "3")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "4")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "5")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "6")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "7")
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "3")
            # time.sleep(5)
            self.st.ui_run("Dashboards_Overview", "1")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            # time.sleep(5)
        except Exception as e:
            self.add_img()
            assert False, e

    # @unittest.skip("已调试通过，跳过")
    def test_Filter_on_taxonomy_label(self):
        '''Filter on Taxonomy Label by Clicking on Widget'''
        """"先创建wiget,再进行filter,用完之后再删除了"""
        try:
            self.st.login_uat()
            # time.sleep(1)
            self.st.ui_run("HomePage_Dashboards", "1")
            # time.sleep(5)
            self.st.ui_run("Dashboards_search", "1")
            self.st.ui_run("Dashboards_search", "3")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            print("create wiget")
            self.st.ui_run("Dashboards_wiget_add", "1")
            time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "1")
            time.sleep(5)
            self.st.ui_run("Dashboards_wiget_editer", "3")
            time.sleep(5)
            self.st.ui_run("Dashboards_wiget_editer", "4")
            time.sleep(5)
            before_filter = self.st.check_filter_count()
            print("before changed the filter, the count is:", before_filter)
            self.st.ui_run("Dashboards_filter", "1")
            time.sleep(2)
            self.st.ui_run("Dashboards_filter", "2")
            time.sleep(2)
            self.st.ui_run("Dashboards_filter", "3")
            time.sleep(2)
            self.st.ui_run("Dashboards_filter", "4")
            time.sleep(5)
            self.st.ui_run("Dashboards_filter", "5")
            time.sleep(5)
            self.st.ui_run("Dashboards_filter", "6")
            time.sleep(5)
            self.st.ui_run("Dashboards_filter", "7")
            time.sleep(5)
            self.st.ui_run("Dashboards_filter", "8")
            time.sleep(5)
            after_fiter = self.st.check_filter_count()
            print("after change the filter, the count is:", after_fiter)
            print("~~~~~remove wiget~~~~~")
            self.st.ui_run("Dashboards_wiget_delete", "1")
            self.st.ui_run("Dashboards_wiget_delete", "2")
            # time.sleep(5)
            assert before_filter != after_fiter

        except Exception as e:
            self.add_img()
            assert False, e

    # @unittest.skip("已调试通过，跳过")
    def test_deploy_analysis_with_two_txonomy_and_verify_remove(self):
        try:
            self.st.login_uat()
            # time.sleep(1)
            self.st.ui_run("HomePage_Dashboards", "1")
            # time.sleep(2)
            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Dashboards_New_Dashboard", "0")
            # time.sleep(2)
            dashboard_tx_name = "txonomy_add_remove" + time_now()
            self.st.ui_run("Dashboards_New_Dashboard", "1", dashboard_tx_name)
            self.st.ui_run("Dashboards_New_Dashboard", "2")
            self.st.ui_run("Dashboards_New_Dashboard", "3")
            time.sleep(3)
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "1")
            time.sleep(1)
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "2")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "3", self.st.lang)
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "1")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_1")
            time.sleep(5)
            print("Taxonomy,选择数据-text格式")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "5", self.st.lang)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "6")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "1")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "2", "one_taxonomy_test")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "3", "one_taxonomy_test")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "4")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "1")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "2", "two_taxonomy_test")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "3", "two_taxonomy_test")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "4")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "5")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "6")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "7")
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "3")
            time.sleep(5)
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            time.sleep(1)
            self.st.ui_run("Dashboards_Overview", "4")
            time.sleep(1)
            self.st.ui_run("Dashboards_Overview", "5")
            time.sleep(5)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "6")
            before_remove = self.st.check_model_count()
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "5")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "5")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "7")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "5")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "8")
            time.sleep(2)
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "5")
            time.sleep(5)
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "4")
            time.sleep(1)
            self.st.ui_run("Dashboards_Overview", "5")
            time.sleep(5)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "6")
            time.sleep(5)
            after_move = self.st.check_model_count()
            print("移除前个数:", before_remove)
            print("移除后个数:", after_move)
            assert before_remove != after_move
        except Exception as e:
            self.add_img()
            assert False, e

    # @unittest.skip("已调试通过，跳过")
    def test_deploy_nlu_one_text_field_and_verify_topic(self):
        try:
            self.st.login_uat()
            # time.sleep(1)
            self.st.ui_run("HomePage_Dashboards", "1")

            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Dashboards_New_Dashboard", "0")
            dashboard_nlu_name = "deploy_nlu1T" + time_now()
            self.st.ui_run("Dashboards_New_Dashboard", "1", dashboard_nlu_name)
            self.st.ui_run("Dashboards_New_Dashboard", "2")
            self.st.ui_run("Dashboards_New_Dashboard", "3")
            time.sleep(3)
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "1")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "2")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "3", self.st.lang)
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "1")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_2")
            time.sleep(5)
            self.st.dashborad_delete_fileds()
            time.sleep(2)
            print("NLU,选择数据-text格式")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1")
            time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "5", self.st.lang)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "6")
            # time.sleep(2)
            nlu_name = "nlu1" + time_now()
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "6", nlu_name)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "7")
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "3")
            time.sleep(5)
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            # time.sleep(5)
            print("create wiget")
            self.st.ui_run("Dashboards_wiget_add", "1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "2")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "4")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "5")
        except Exception as e:
            self.add_img()
            assert False, e

    # @unittest.skip("已调试通过，跳过")
    def test_deploy_nlu_two_text_field_and_verify_topic(self):
        try:
            self.st.login_uat()
            # time.sleep(1)
            self.st.ui_run("HomePage_Dashboards", "1")

            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Dashboards_New_Dashboard", "0")
            dashboard_tx_name = "deploy_nlu2T" + time_now()
            self.st.ui_run("Dashboards_New_Dashboard", "1", dashboard_tx_name)
            self.st.ui_run("Dashboards_New_Dashboard", "2")
            self.st.ui_run("Dashboards_New_Dashboard", "3")
            time.sleep(3)
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "1")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "2")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "3", self.st.lang)
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "1")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_2")
            time.sleep(5)
            self.st.dashborad_delete_fileds()
            time.sleep(2)
            print("NLU,选择数据-text格式")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1")
            time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            print("NLU,选择数据-date格式")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "2")
            time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "4")
            time.sleep(1)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "5", self.st.lang)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "6")
            # time.sleep(2)
            nlu_name = "nlu2" + time_now()
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "6", nlu_name)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "7")
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "3")
            time.sleep(5)
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            # time.sleep(5)
            print("create wiget")
            self.st.ui_run("Dashboards_wiget_add", "1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "2")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "4")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "5")
        except Exception as e:
            self.add_img()
            assert False, e

    def test_tune_stopword_and_verify_remove(self):  # 基于NLU的模型
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_stopword_tune")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_stopword_tune")
            self.st.ui_run("Dashboards_search", "4")

            print("----check the Data process Status and the buzzword----")
            time.sleep(5)
            self.st.overview_open("Data")
            time.sleep(2)
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            # self.st.ui_run("Dashboards_Overview", "7")

            print("----add the stopwords and reprocess then check the buzzword----")
            self.st.overview_open("Tune")
            self.st.ui_run("Dashboards_Overview", "6", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "8")
            self.st.ui_run("Dashboards_Modify_Advanced_Settings", "1_1")
            self.st.ui_run("Dashboards_Modify_Advanced_Settings", "2_0")
            time.sleep(2)
            # self.st.ui_run("Dashboards_Modify_Advanced_Settings", "2")
            print(" add list:DK_stopword_test")
            check_status = self.st.find_by("xpath", "//a[text()='DK_stopword_test']/ancestor::div[@class='notification-message']/div/i")
            if check_status.get_attribute("class") != "checked":
                check_status.click()
            self.st.ui_run("Dashboards_Modify_Advanced_Settings", "3")
            time.sleep(2)
            self.st.ui_run("Dashboards_Modify_Advanced_Settings", "4")
            time.sleep(5)
            self.st.web_refresh()
            print("----check the Data process status and the buzzword----")
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            # self.st.ui_run("Dashboards_Overview", "7", "好好好好")

            print("----remove the stopwords and reprocess then check the buzzword----")
            self.st.overview_open("Tune")
            self.st.ui_run("Dashboards_Overview", "8")
            self.st.ui_run("Dashboards_Modify_Advanced_Settings", "6")
            self.st.ui_run("Dashboards_Modify_Advanced_Settings", "4")
            time.sleep(5)
            self.st.web_refresh()
            time.sleep(2)
            print("----check the Data process status and the buzzword----")
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            # self.st.ui_run("Dashboards_Overview", "7")

        except Exception as e:
            self.add_img()
            assert False, e

    def test_tune_sentiment_and_reprocess(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_sentiment_tune")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_sentiment_tune")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            print("----check the Data Ingestion Status----")
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)

            print("----add the sentiment and reprocess----")
            self.st.overview_open("Tune")
            self.st.ui_run("Dashboards_Overview", "6", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "8")
            self.st.ui_run("Dashboards_Modify_Advanced_Settings", "1_2")
            self.st.ui_run("Dashboards_Modify_Advanced_Settings", "2_0", "dk_sentiment_test")
            time.sleep(2)
            # self.st.ui_run("Dashboards_Modify_Advanced_Settings", "5")
            print(" add list:dk_sentiment_test")
            check_status = self.st.find_by("xpath", "//a[text()='dk_sentiment_test']/ancestor::div[@class='notification-message']/div/i")
            if check_status.get_attribute("class") != "checked":
                check_status.click()
            self.st.ui_run("Dashboards_Modify_Advanced_Settings", "3")
            self.st.ui_run("Dashboards_Modify_Advanced_Settings", "4")
            time.sleep(5)
            self.st.web_refresh()
            time.sleep(2)
            self.st.overview_open("Data")
            print("----check the Data process status----")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            print("----remove the sentiment and reprocess----")
            self.st.overview_open("Tune")
            self.st.ui_run("Dashboards_Overview", "8")
            self.st.ui_run("Dashboards_Modify_Advanced_Settings", "7")
            self.st.ui_run("Dashboards_Modify_Advanced_Settings", "4")
            time.sleep(5)
            self.st.web_refresh()
            self.st.overview_open("Data")
            print("----check the Data process status----")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)

        except Exception as e:
            self.add_img()
            assert False, e


    def test_filter_on_buzzword_by_clicking_on_Widget(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "filter_on_buzzword")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "filter_on_buzzword")
            self.st.ui_run("Dashboards_search", "4")

            print("----check the Data process Status----")
            self.st.ui_run("Dashboards_Overview", "1")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)

            print("----remove the widget----")
            self.st.remove_the_second_widget()
            print("----create a new widget with all----")
            self.st.ui_run("Dashboards_wiget_add", "1")
            self.st.ui_run("Dashboards_wiget_editer", "2_7")
            self.st.ui_run("Dashboards_wiget_editer", "3")
            self.st.ui_run("Dashboards_wiget_editer", "4")
            # self.st.ui_run("Dashboards_wiget_editer", "5")
            time.sleep(5)

            print("----before filter, check the number----")
            self.st.ui_run("dashboard_widget", "3_1", "17")
            self.st.ui_run("dashboard_widget", "3_2", "9")
            self.st.ui_run("dashboard_widget", "3_3", "20")
            self.st.ui_run("dashboard_widget", "3_4", "5")

            print("----change the filter for buzzword----")
            self.st.ui_run("dashboard_widget", "1")
            self.st.ui_run("dashboard_widget", "2_3")
            self.st.ui_run("Dashboards_wiget_editer", "10_2")
            self.st.ui_run("Dashboards_wiget_editer", "4")
            time.sleep(5)

            print("----After filter, check the number----")
            self.st.ui_run("dashboard_widget", "3_1", "5")
            self.st.ui_run("dashboard_widget", "3_2", "12")
            self.st.ui_run("dashboard_widget", "3_3", "4")
            self.st.ui_run("dashboard_widget", "3_4", "3")
        except Exception as e:
            self.add_img()
            assert False, e

    def test_deploy_NSM_one_text(self):
        try:
            self.st.login_uat()
            # time.sleep(1)
            self.st.ui_run("HomePage_Dashboards", "1")

            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Dashboards_New_Dashboard", "0")
            dashboard_nsm_name = "deploy_NSM1T" + time_now()
            self.st.ui_run("Dashboards_New_Dashboard", "1", dashboard_nsm_name)
            self.st.ui_run("Dashboards_New_Dashboard", "2")
            self.st.ui_run("Dashboards_New_Dashboard", "3")
            time.sleep(3)
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "1")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "2")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "3", self.st.lang)
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "1")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_3")
            time.sleep(5)
            print("Neural Sentiment Analysis(Travel),选择数据-text格式")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "5", self.st.lang)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "6")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "6", "Neural Sentiment Analysis:one")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "7")
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "3")
            time.sleep(5)
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            # time.sleep(5)
            self.st.ui_run("Dashboards_wiget_add", "1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "2_1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "4")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "5_1")
        except Exception as e:
            self.add_img()
            assert False, e

    def test_deploy_NSM_two_text(self):
        try:
            self.st.login_uat()
            # time.sleep(1)
            self.st.ui_run("HomePage_Dashboards", "1")
            # time.sleep(2)
            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Dashboards_New_Dashboard", "0")
            # time.sleep(2)
            dashboard_nsm_name = "deploy_NSM2T" + time_now()
            self.st.ui_run("Dashboards_New_Dashboard", "1", dashboard_nsm_name)
            self.st.ui_run("Dashboards_New_Dashboard", "2")
            self.st.ui_run("Dashboards_New_Dashboard", "3")
            time.sleep(3)
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "1")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "2")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "3", self.st.lang)
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "1")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_3")
            time.sleep(5)
            print("one:Neural Sentiment Analysis(Travel),选择数据-text格式")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            # time.sleep(1)
            print("two:Neural Sentiment Analysis(Travel),选择数据-text格式")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1_1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "5", self.st.lang)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "6")
            # time.sleep(2)

            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "6", "Neural Sentiment Analysis:two")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "7")
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "3")
            time.sleep(5)
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            # time.sleep(5)
            print("create wiget")
            self.st.ui_run("Dashboards_wiget_add", "1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "2_1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "4")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "5_1")

        except Exception as e:
            self.add_img()
            assert False, e

    # @unittest.skip("已调试通过，跳过")
    def test_deploy_semi_auto_txonomy_one_text(self):
        try:
            self.st.login_uat()
            # time.sleep(1)
            self.st.ui_run("HomePage_Dashboards", "1")

            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Dashboards_New_Dashboard", "0")
            dashboard_tx_name = "deploy_txonomy1T" + time_now()
            self.st.ui_run("Dashboards_New_Dashboard", "1", dashboard_tx_name)
            self.st.ui_run("Dashboards_New_Dashboard", "2")
            self.st.ui_run("Dashboards_New_Dashboard", "3")
            time.sleep(3)
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "1")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "2")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "3", self.st.lang)
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "1")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_4")
            time.sleep(5)
            print("semi_auto_Taxonomy,选择数据-text格式")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "5", self.st.lang)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "6")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "1")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "2", "semi_auto_taxonomy")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "3", "semi_auto_taxonomy")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "4")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "5")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "6")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "7")
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "3")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Overview", "1")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            # time.sleep(5)
        except Exception as e:
            self.add_img()
            assert False, e

    # @unittest.skip("已调试通过，跳过")
    def test_deploy_semi_auto_txonomy_two_text(self):
        try:
            self.st.login_uat()
            # time.sleep(1)
            self.st.ui_run("HomePage_Dashboards", "1")
            # time.sleep(2)
            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Dashboards_New_Dashboard", "0")
            # time.sleep(2)
            dashboard_tx_name = "deploy_txonomy2T" + time_now()
            # dashboard_tx_name = "DK99TEST"
            self.st.ui_run("Dashboards_New_Dashboard", "1", dashboard_tx_name)
            self.st.ui_run("Dashboards_New_Dashboard", "2")
            self.st.ui_run("Dashboards_New_Dashboard", "3")
            time.sleep(3)
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "1")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "2")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "3", self.st.lang)
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "1")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_1")
            time.sleep(5)
            print("semi_auto_Taxonomy,选择数据-text格式")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            # time.sleep(1)
            print("semi_auto_Taxonomy,选择数据-text格式")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1_1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "5", self.st.lang)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "6")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "1")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "2", "semi_auto_taxonomy")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "3", "semi_auto_taxonomy")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "4")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "5")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "6")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "7")
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "3")
            # time.sleep(5)
            self.st.ui_run("Dashboards_Overview", "1")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            # time.sleep(5)
        except Exception as e:
            self.add_img()
            assert False, e

    def test_feedfback_loop_trigger_retrain_and_reprocess(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_semi_auto_taxonomy")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_semi_auto_taxonomy")
            self.st.ui_run("Dashboards_search", "4")

            print("----check the Data process Status and the buzzword----")
            time.sleep(5)
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)

            print("----open the tune panel----")
            self.st.ui_run("Dashboards_Overview", "1_1")
            self.st.ui_run("Dashboards_Overview", "6", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "8")
            print("----reprocess----")
            self.st.ui_run("Dashboards_Modify_Advanced_Settings", "4")
            time.sleep(5)

            print("----after reprocess,check the Data process Status-----")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
        except Exception as e:
            self.add_img()
            assert False, e

    def test_deploy_geo_model_and_verify_wiget(self):
        try:
            self.st.login_uat()
            # time.sleep(1)
            self.st.ui_run("HomePage_Dashboards", "1")

            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Dashboards_New_Dashboard", "0")
            dashboard_geo_name = "deploy_geo" + time_now()
            self.st.ui_run("Dashboards_New_Dashboard", "1", dashboard_geo_name)
            self.st.ui_run("Dashboards_New_Dashboard", "2")
            self.st.ui_run("Dashboards_New_Dashboard", "3")
            time.sleep(3)
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "1")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "2")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "3", self.st.lang)
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "1")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_5")
            print("Geo tag,选择数据")
            time.sleep(5)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1_2")
            time.sleep(1)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "4_1")
            time.sleep(1)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1_3")
            time.sleep(1)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "4_2")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "5", self.st.lang)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "6")
            time.sleep(1)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "6", "geo tag model test")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "7")
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "3")
            time.sleep(5)
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            # time.sleep(5)
            #
            # print("----Data Ingestion:add autolearn/NLU/taxonomy model----")
            # self.st.ui_run("Dashboards_Overview", "1")   # 点击dropdown（三个小点）的图标
            # self.st.ui_run("Dashboards_Overview", "2")   # 点击编辑
            # self.st.ui_run("Dashboards_Select_Deploy_Mode", "1")  # 点击部署模型
            #
            # self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_1")  # 选择模型：Taxonomy Analysis
            # print("Taxonomy,选择维度")
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1")
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "5", self.st.lang)
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "6")
            # # time.sleep(2)
            # print("选择定向模型")
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "1")
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "2", "one_taxonomy_test")
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "3", "one_taxonomy_test")
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "4")
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "5")  # 选择模型并提交
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "7")
            #
            # self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_2")  # 选择模型：NLU Analysis

            print("----create wiget and verify----")
            self.st.ui_run("Dashboards_wiget_add", "1")
            self.st.ui_run("Dashboards_wiget_editer", "2_2")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "4")
            # time.sleep(2)
            # self.st.ui_run("Dashboards_wiget_editer", "5")
        except Exception as e:
            self.add_img()
            assert False, e

    def test_verify_global_filters_on_all_models(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_dashbaord_filter")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_dashbaord_filter")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(5)
            self.st.overview_open("Data")
            if self.st.lang == "lang-en":
                self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
                self.st.ui_run("Dashboards_Overview", "3", "6 models up-to-date")
            if self.st.lang == "lang-zh":
                self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
                self.st.ui_run("Dashboards_Overview", "3", "6个模型已经部署完毕")
            time.sleep(5)
            print("-----remove bottom filters----")
            self.st.remove_bottom_filter()
            print("----create a global filter----")
            time.sleep(2)
            self.st.overview_open("Filters")
            time.sleep(5)
            print("add global filter on NLU")
            self.st.ui_run("Dashboards_Overview_filter", "1")
            time.sleep(1)
            self.st.ui_run("Dashboards_Overview_filter", "2")
            self.st.ui_run("Dashboards_Overview_filter", "3")
            self.st.ui_run("Dashboards_Overview_filter", "4")
            print("verify global nlu filter condition effect")
            self.st.ui_run("Dashboards_Overview", "9", '2,056')
            time.sleep(2)
            self.st.overview_open("Filters")
            time.sleep(5)
            print("add global filter on Geo")
            self.st.ui_run("Dashboards_Overview_filter", "1")
            time.sleep(1)
            self.st.ui_run("Dashboards_Overview_filter", "2_1")
            self.st.ui_run("Dashboards_Overview_filter", "3")
            self.st.ui_run("Dashboards_Overview_filter", "4")
            print("verify global nlu filter condition effect")
            self.st.ui_run("Dashboards_Overview", "9", '481')

            time.sleep(2)
            self.st.overview_open("Filters")
            time.sleep(5)
            print("add global filter on taxonomy")
            self.st.ui_run("Dashboards_Overview_filter", "1")
            time.sleep(1)
            self.st.ui_run("Dashboards_Overview_filter", "2_2")
            self.st.ui_run("Dashboards_Overview_filter", "5")
            self.st.ui_run("Dashboards_Overview_filter", "6")
            self.st.ui_run("Dashboards_Overview_filter", "4")
            print("verify global taxonomy filter condition effect")
            self.st.ui_run("Dashboards_Overview", "9", '38')
        except Exception as e:
            self.add_img()
            assert False, e

    def test_apply_tab_filter_and_global_filter(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_dashbaord_filter")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_dashbaord_filter")
            self.st.ui_run("Dashboards_search", "4")
            self.st.web_refresh()
            time.sleep(20)
            print("-----remove bottom filters(global and tab)----")
            self.st.remove_bottom_filter()
            print("----create a global filter----")
            self.st.overview_open("Filters")
            print("add global filter on NLU")
            self.st.ui_run("Dashboards_Overview_filter", "1")
            self.st.ui_run("Dashboards_Overview_filter", "2")
            self.st.ui_run("Dashboards_Overview_filter", "3")
            self.st.ui_run("Dashboards_Overview_filter", "4")
            print("verify global nlu filter condition effect")
            self.st.ui_run("Dashboards_Overview", "9", '2,056')

            print("----create a tab filter----")
            self.st.ui_run("Dashboards_Overview", "10")  # 点击概览旁边的小三角切换按钮
            self.st.ui_run("Dashboards_Overview_tab_filter", "1")
            self.st.ui_run("Dashboards_Overview_tab_filter", "2")
            self.st.ui_run("Dashboards_Overview_tab_filter", "3_1")
            self.st.ui_run("Dashboards_Overview_tab_filter", "4")
            self.st.ui_run("Dashboards_Overview_tab_filter", "5")
            self.st.ui_run("Dashboards_Overview_tab_filter", "6")
            print("verify tab_filter and global filter effect")
            self.st.ui_run("Dashboards_Overview", "9", '481')
        except Exception as e:
            self.add_img()
            assert False, e

    def test_apply_tab_filter_overwrite_global_filter(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_dashbaord_filter")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_dashbaord_filter")
            self.st.ui_run("Dashboards_search", "4")
            self.st.web_refresh()
            time.sleep(20)
            print("-----remove bottom filters(global and tab)----")
            self.st.remove_bottom_filter()
            print("----create a global filter----")
            self.st.overview_open("Filters")
            print("add global filter on NLU")
            self.st.ui_run("Dashboards_Overview_filter", "1")
            self.st.ui_run("Dashboards_Overview_filter", "2")
            self.st.ui_run("Dashboards_Overview_filter", "3")
            self.st.ui_run("Dashboards_Overview_filter", "4")
            print("verify global nlu filter condition effect")
            self.st.ui_run("Dashboards_Overview", "9", '2,056')

            print("----create a tab filter----")
            self.st.ui_run("Dashboards_Overview", "10")  # 点击概览旁边的小三角切换按钮
            self.st.ui_run("Dashboards_Overview_tab_filter", "1")
            self.st.ui_run("Dashboards_Overview_tab_filter", "2")
            self.st.ui_run("Dashboards_Overview_tab_filter", "3_1")
            self.st.ui_run("Dashboards_Overview_tab_filter", "4")
            self.st.ui_run("Dashboards_Overview_tab_filter", "5")
            select = self.st.find_by("css", "input[type='checkbox']")
            if not select.is_selected():
                select.click()
            self.st.ui_run("Dashboards_Overview_tab_filter", "6")
            print("verify tab_filter and global filter effect")
            self.st.ui_run("Dashboards_Overview", "9", '516')
        except Exception as e:
            self.add_img()
            assert False, e

    def test_apply_wiget_filter_and_other_fitler(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_dashbaord_filter")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_dashbaord_filter")
            self.st.ui_run("Dashboards_search", "4")
            self.st.web_refresh()
            time.sleep(20)
            print("-----remove bottom filters(global and tab)----")
            self.st.remove_bottom_filter()
            print("----create a global filter----")
            self.st.overview_open("Filters")
            print("add global filter on NLU")
            self.st.ui_run("Dashboards_Overview_filter", "1")
            self.st.ui_run("Dashboards_Overview_filter", "2")
            self.st.ui_run("Dashboards_Overview_filter", "3")
            self.st.ui_run("Dashboards_Overview_filter", "4")
            print("verify global nlu filter condition effect")
            self.st.ui_run("Dashboards_Overview", "9", '2,056')

            print("----create a tab filter----")
            self.st.ui_run("Dashboards_Overview", "10")  # 点击概览旁边的小三角切换按钮
            self.st.ui_run("Dashboards_Overview_tab_filter", "1")
            self.st.ui_run("Dashboards_Overview_tab_filter", "2")
            self.st.ui_run("Dashboards_Overview_tab_filter", "3_1")
            self.st.ui_run("Dashboards_Overview_tab_filter", "4")
            self.st.ui_run("Dashboards_Overview_tab_filter", "5")
            self.st.ui_run("Dashboards_Overview_tab_filter", "6")
            print("verify tab_filter and global filter effect")
            self.st.ui_run("Dashboards_Overview", "9", '481')

            print("----create a widget filter----")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "1")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "2")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "3")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "4")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "5")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "6")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "7")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "8")
            print("verify widget taxonomy filter condition effect")
            self.st.ui_run("Dashboards_Overview", "9", '38')

        except Exception as e:
            self.add_img()
            assert False, e

    def test_apply_wiget_filter_overwrite_other_fitler(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_dashbaord_filter")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_dashbaord_filter")
            self.st.ui_run("Dashboards_search", "4")
            self.st.web_refresh()
            time.sleep(20)
            print("-----remove bottom filters(global and tab)----")
            self.st.remove_bottom_filter()
            print("----create a global filter----")
            self.st.overview_open("Filters")
            print("add global filter on NLU")
            self.st.ui_run("Dashboards_Overview_filter", "1")
            self.st.ui_run("Dashboards_Overview_filter", "2")
            self.st.ui_run("Dashboards_Overview_filter", "3")
            self.st.ui_run("Dashboards_Overview_filter", "4")
            print("verify global nlu filter condition effect")
            self.st.ui_run("Dashboards_Overview", "9", '2,056')

            print("----create a tab filter----")
            self.st.ui_run("Dashboards_Overview", "10")  # 点击概览旁边的小三角切换按钮
            self.st.ui_run("Dashboards_Overview_tab_filter", "1")
            self.st.ui_run("Dashboards_Overview_tab_filter", "2")
            self.st.ui_run("Dashboards_Overview_tab_filter", "3_1")
            self.st.ui_run("Dashboards_Overview_tab_filter", "4")
            self.st.ui_run("Dashboards_Overview_tab_filter", "5")
            self.st.ui_run("Dashboards_Overview_tab_filter", "6")
            print("verify tab_filter and global filter effect")
            self.st.ui_run("Dashboards_Overview", "9", '481')

            print("----create a widget filter----")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "1")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "2")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "3")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "4")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "5")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "6")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "7")
            select = self.st.find_by("css", "input[type='checkbox']")
            if not select.is_selected():
                select.click()
            self.st.ui_run("Dashboards_Overview_wiget_filter", "8")
            print("verify widget taxonomy filter condition effect")
            self.st.ui_run("Dashboards_Overview", "9", '168')

        except Exception as e:
            self.add_img()
            assert False, e

    # @unittest.skip("skipped because the bug is not fixed:https://stratifyd.atlassian.net/browse/CORE-1884")
    def test_apply_global_filter_exclude_empty_value(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_exclude_empty_value")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_exclude_empty_value")
            self.st.ui_run("Dashboards_search", "4")
            self.st.web_refresh()
            time.sleep(20)
            self.st.ui_run("Dashboards_Overview", "9", '1,096')  # 检查页面加载完毕
            print("-----remove bottom filters----")
            self.st.remove_bottom_filter()
            print("----create a global filter----")
            self.st.overview_open("Filters")
            print("add global filter exclude empty values")
            self.st.ui_run("Dashboards_Overview_filter", "1")
            self.st.ui_run("Dashboards_Overview_filter", "2_3")
            self.st.ui_run("Dashboards_Overview_filter", "7")
            self.st.ui_run("Dashboards_Overview_filter", "8")
            self.st.ui_run("Dashboards_Overview_filter", "4")  # 提交
            print("verify global nlu filter condition effect")
            self.st.ui_run("Dashboards_Overview", "9", '1,095')
        except Exception as e:
            self.add_img()
            assert False, e

    def test_apply_tab_filter_exclude_empty_value(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_exclude_empty_value")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_exclude_empty_value")
            self.st.ui_run("Dashboards_search", "4")
            self.st.ui_run("Dashboards_Overview", "9", '1,096')  # 检查页面加载完毕
            self.st.web_refresh()
            time.sleep(20)
            print("-----remove bottom filters----")
            self.st.remove_bottom_filter()
            print("----create a tab filter----")
            self.st.ui_run("Dashboards_Overview", "10")  # 点击概览旁边的小三角切换按钮
            self.st.ui_run("Dashboards_Overview_tab_filter", "1")
            self.st.ui_run("Dashboards_Overview_tab_filter", "2")
            self.st.ui_run("Dashboards_Overview_tab_filter", "3_2")
            self.st.ui_run("Dashboards_Overview_tab_filter", "7")
            self.st.ui_run("Dashboards_Overview_tab_filter", "8")
            self.st.ui_run("Dashboards_Overview_tab_filter", "6")
            print("verify tab_filter and global filter effect")
            self.st.ui_run("Dashboards_Overview", "9", '1,095')
        except Exception as e:
            self.add_img()
            assert False, e

    def test_apply_widget_filter_exclude_empty_value(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_exclude_empty_value")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_exclude_empty_value")
            self.st.ui_run("Dashboards_search", "4")
            self.st.web_refresh()
            time.sleep(20)
            self.st.ui_run("Dashboards_Overview", "9", '1,096')  # 检查页面加载完毕
            print("-----remove bottom filters----")
            self.st.remove_bottom_filter()
            time.sleep(5)
            print("----create a widget filter----")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "1")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "2")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "3")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "4_1")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "10")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "11")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "7")
            self.st.ui_run("Dashboards_Overview_wiget_filter", "8")
            print("verify widget taxonomy filter condition effect")
            self.st.ui_run("Dashboards_Overview", "9", '1,095')
        except Exception as e:
            self.add_img()
            assert False, e

    def test_verify_field_type_number_and_text(self):
        try:
            print("----open widget and check the origin data field type-----")
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_verify_field")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_verify_field")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            print("-----Enter the create widget page----")
            self.st.ui_run("Dashboards_wiget_add", "1")
            print("verify the data_filed")
            self.st.ui_run("Dashboards_wiget_editer", "2_3")
            self.st.ui_run("Dashboards_wiget_editer", "6")
            self.st.ui_run("Dashboards_wiget_editer", "7")

            print("apply the text_filed sucess")
            self.st.ui_run("Dashboards_wiget_editer", "6_1")
            self.st.ui_run("Dashboards_wiget_editer", "2_4")
            self.st.ui_run("Dashboards_wiget_editer", "7_1")


        except Exception as e:
            self.add_img()
            assert False, e

    def test_verify_filed_type_temporal(self):
        try:
            print("----open widget and check the origin data field type-----")
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_verify_field")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_verify_field")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            print("-----Enter the create widget page----")
            self.st.ui_run("Dashboards_wiget_add", "1")
            print("verify the temporal_filed")
            self.st.ui_run("Dashboards_wiget_editer", "2_5")
            self.st.ui_run("Dashboards_wiget_editer", "6")
            self.st.ui_run("Dashboards_wiget_editer", "7_1")

        except Exception as e:
            self.add_img()
            assert False, e

    def test_verify_aggregation_distinct_count_for_text_field(self):
        try:
            print("----open widget and check the origin data field type-----")
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_verify_field")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_verify_field")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            print("-----Enter the create widget page----")
            self.st.ui_run("Dashboards_wiget_add", "1")
            print("distinct count for text_filed")
            self.st.ui_run("Dashboards_wiget_editer", "2_6")
            self.st.ui_run("Dashboards_wiget_editer", "6")
            self.st.ui_run("Dashboards_wiget_editer", "8")
            time.sleep(5)
            self.st.ui_run("Dashboards_wiget_editer", "9")
        except Exception as e:
            self.add_img()
            assert False, e

    def test_verify_aggregation_nps_cast_for_numerical_field(self):
        try:
            print("----open widget and check the origin data field type-----")
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_verify_field")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_verify_field")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            print("-----Enter the create widget page----")
            self.st.ui_run("Dashboards_wiget_add", "1")
            print("NPS for numerical_field")
            self.st.ui_run("Dashboards_wiget_editer", "2_3")
            self.st.ui_run("Dashboards_wiget_editer", "6")
            time.sleep(1)
            self.st.ui_run("Dashboards_wiget_editer", "8", "6")
            time.sleep(1)
            self.st.ui_run("Dashboards_wiget_editer", "9", "-1")
            time.sleep(1)
            print("CAST 10 for numerical_field")
            # self.st.ui_run("Dashboards_wiget_editer", "6")
            self.st.ui_run("Dashboards_wiget_editer", "8", "9")
            time.sleep(1)
            self.st.ui_run("Dashboards_wiget_editer", "9", "0.5")
        except Exception as e:
            self.add_img()
            assert False, e

    def test_verify_aggregation_avg_sum_for_numerical_field(self):
        try:
            print("----open widget and check the origin data field type-----")
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_verify_field")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_verify_field")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            print("-----Enter the create widget page----")
            self.st.ui_run("Dashboards_wiget_add", "1")
            print("AVG for numerical_field")
            self.st.ui_run("Dashboards_wiget_editer", "2_3")
            self.st.ui_run("Dashboards_wiget_editer", "6")
            self.st.ui_run("Dashboards_wiget_editer", "8", "1")
            self.st.ui_run("Dashboards_wiget_editer", "9", "3.5")
            print("SUM for numerical_field")
            # self.st.ui_run("Dashboards_wiget_editer", "2_3")
            # self.st.ui_run("Dashboards_wiget_editer", "6")
            self.st.ui_run("Dashboards_wiget_editer", "8", "2")
            self.st.ui_run("Dashboards_wiget_editer", "9", "3,849")
        except Exception as e:
            self.add_img()
            assert False, e

    def test_replace_dataStreams_and_verify_reprocess_and_filter(self):
        try:
            print("----open widget and check the origin data field type-----")
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_replace_datastreams")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_replace_datastreams")
            self.st.ui_run("Dashboards_search", "4")

            print("切换数据流--1096")
            self.st.overview_open("Data")  # 打开概览-数据管理
            time.sleep(5)
            chk_1096 = self.st.find_by("css", "h5>span").text
            if chk_1096 != "1096datastreams":
                self.st.ui_run("Dashboards_Overview", "4")
                self.st.ui_run("Dashboards_Overview", "5_1")  # 点击替换
                self.st.ui_run("Dashboards_Overview_replace_data", "1", "1096datastreams")
                self.st.ui_run("Dashboards_Overview_replace_data", "2", "1096datastreams")
                self.st.ui_run("Dashboards_Overview_replace_data", "3")
                self.st.ui_run("Dashboards_Overview_replace_data", "4")  # 点击下一步
                self.st.ui_run("Dashboards_Overview_replace_data", "4")  # 点击下一步
                self.st.ui_run("Dashboards_Overview_replace_data", "5")  # 点击提交
                time.sleep(5)
                self.st.web_refresh()
                print("----verify reprocessing status----")
                self.st.overview_open("Data")  # 打开概览-数据管理
                if self.st.lang == "lang-en":
                    self.st.ui_run("Dashboards_Overview", "3", "2 models up-to-date")
                if self.st.lang == "lang-zh":
                    self.st.ui_run("Dashboards_Overview", "3", "2个模型已经部署完毕")
                time.sleep(5)

            print("1096 DataStreams ,check the data numbers")
            self.st.ui_run("Dashboards_Overview", "9", '258')


            print("切换数据流--2192")
            self.st.overview_open("Data")  # 打开概览-数据管理
            self.st.ui_run("Dashboards_Overview", "4")
            self.st.ui_run("Dashboards_Overview", "5_1")  # 点击替换
            self.st.ui_run("Dashboards_Overview_replace_data", "1")
            self.st.ui_run("Dashboards_Overview_replace_data", "2")
            self.st.ui_run("Dashboards_Overview_replace_data", "3")
            self.st.ui_run("Dashboards_Overview_replace_data", "4")  # 点击下一步
            self.st.ui_run("Dashboards_Overview_replace_data", "4")  # 点击下一步
            self.st.ui_run("Dashboards_Overview_replace_data", "5")  # 点击提交
            time.sleep(5)
            self.st.web_refresh()
            print("----verify reprocessing status----")
            self.st.overview_open("Data")  # 打开概览-数据管理
            if self.st.lang == "lang-en":
                self.st.ui_run("Dashboards_Overview", "3", "2 models up-to-date")
            if self.st.lang == "lang-zh":
                self.st.ui_run("Dashboards_Overview", "3", "2个模型已经部署完毕")
            time.sleep(5)
            print("----verify filter update success----")
            print("change to 2192 DataStreams ,check the data numbers")
            self.st.ui_run("Dashboards_Overview", "9", '516')

        except Exception as e:
            self.add_img()
            assert False, e

    def test_confirm_data_tab_has_tabs_for_all_models(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "dk_all_model")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "dk_all_model")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            self.st.ui_run("Dashboards_Data", "1")  # 点击数据（Data）
            time.sleep(10)
            read_all_models = []
            all_element = self.st.driver.find_elements_by_css_selector(".tab-container>.tab span")
            for element in all_element:
                read_all_models.append(element.text)
            if self.st.lang == "lang-zh":
                expect_model = ["NLU", "taxonomy", "autolearn", "semi_taxonomy","geo" ,"全"]
            if self.st.lang == "lang-en":
                expect_model = ["NLU", "taxonomy", "autolearn", "semi_taxonomy","geo",  "All"]
            for model in expect_model:
                if model not in read_all_models:
                    assert False, model + " not in " + read_all_models
        except Exception as e:
            self.add_img()
            assert False, e


    def test_export_excel_from_widget_and_get_count(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "test_data_export")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "test_data_export")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            self.st.ui_run("dashboard_widget", "1")  # 选择第二个组件
            self.st.ui_run("dashboard_widget", "2_2")  # 点击导出按钮
            self.st.ui_run("dashboard_widget_export", "1_1")
            file_name="download_excel"+time_now()
            self.st.ui_run("dashboard_widget_export", "2", file_name)
            self.st.ui_run("dashboard_widget_export", "3")
            export_path = download_path + "/" +file_name+ ".xlsx"
            time.sleep(10)
            number = get_excel_row_number(export_path)
            print("except number:", 31, "\n read number:", number)
            assert 31 == number, "the file rows count is wrong"


        except Exception as e:
            self.add_img()
            assert False, e

    def test_export_csv_from_widget_and_get_count(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "test_data_export")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "test_data_export")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            self.st.ui_run("dashboard_widget", "1")  # 选择第二个组件
            self.st.ui_run("dashboard_widget", "2_2")  # 点击导出按钮
            self.st.ui_run("dashboard_widget_export", "1_2")
            file_name = "download_csv"+time_now()
            self.st.ui_run("dashboard_widget_export", "2", file_name)
            self.st.ui_run("dashboard_widget_export", "3")
            time.sleep(10)
            export_path = download_path + "/" +file_name+ ".csv"
            number = get_csv_row_number(export_path)
            print("except number:", 31, "\n read number:", number)
            assert 31 == number, "the file rows count is wrong"

        except Exception as e:
            self.add_img()
            assert False, e

    def test_export_jsonl_from_widget_and_get_count(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "test_data_export")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "test_data_export")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            self.st.ui_run("dashboard_widget", "1")  # 选择第二个组件
            self.st.ui_run("dashboard_widget", "2_2")  # 点击导出按钮
            self.st.ui_run("dashboard_widget_export", "1_2")
            self.st.ui_run("dashboard_widget_export", "2", "download_jsonl"+time_now())
            self.st.ui_run("dashboard_widget_export", "3")

        except Exception as e:
            self.add_img()
            assert False, e

    def test_export_graph_from_widget_and_get_count(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "test_data_export")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "test_data_export")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            self.st.ui_run("dashboard_widget", "1")  # 选择第二个组件
            self.st.ui_run("dashboard_widget", "2_2")  # 点击导出按钮
            self.st.ui_run("dashboard_widget_export", "1_2")
            self.st.ui_run("dashboard_widget_export", "2", "download_graph"+time_now())
            self.st.ui_run("dashboard_widget_export", "3")

        except Exception as e:
            self.add_img()
            assert False, e

    def test_export_stream_from_data_taxonomy_tab(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "test_data_export")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "test_data_export")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            self.st.ui_run("Dashboards_Data", "1")  # 点击数据（Data）
            time.sleep(1)
            self.st.ui_run("Dashboards_Data", "2_1")
            self.st.ui_run("Dashboards_Data", "3_2")
            self.st.ui_run("Dashboards_Data", "4")
            self.st.ui_run("Dashboards_Data", "5", "export_taxonomy_stream"+time_now())
            self.st.ui_run("Dashboards_Data", "6")
        except Exception as e:
            self.add_img()
            assert False, e

    def test_export_stream_from_data_semi_taxonomy_tab(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "test_data_export")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "test_data_export")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            self.st.ui_run("Dashboards_Data", "1")  # 点击数据（Data）
            time.sleep(1)
            self.st.ui_run("Dashboards_Data", "2_2")
            self.st.ui_run("Dashboards_Data", "3")
            self.st.ui_run("Dashboards_Data", "4")
            self.st.ui_run("Dashboards_Data", "5", "export_semi_taxonomy_stream"+time_now())
            self.st.ui_run("Dashboards_Data", "6")
        except Exception as e:
            self.add_img()
            assert False, e

    def test_export_stream_from_data_all_tab(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "test_data_export")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "test_data_export")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(5)
            self.st.ui_run("Dashboards_Data", "1")  # 点击数据（Data）
            time.sleep(1)
            self.st.ui_run("Dashboards_Data", "2_3")
            time.sleep(1)
            self.st.ui_run("Dashboards_Data", "3_1")
            self.st.ui_run("Dashboards_Data", "4")
            self.st.ui_run("Dashboards_Data", "5", "export_all_stream"+time_now())
            self.st.ui_run("Dashboards_Data", "6")
        except Exception as e:
            self.add_img()
            assert False, e

    def test_export_pdf(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "test_data_export")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "test_data_export")
            self.st.ui_run("Dashboards_search", "4")
            print("----dashboard export pdf----")
            self.st.ui_run("Dashboards", "1")
            self.st.ui_run("Dashboards", "2_1")
            self.st.ui_run("Dashboards", "3")
            self.st.ui_run("Dashboards_Export", "1")
            self.st.ui_run("Dashboards_Export", "2")
            self.st.ui_run("Dashboards_Export", "3")
        except Exception as e:
            self.add_img()
            assert False, e

    def test_verify_search_in_data_tab(self):
        try:
            print("----login and click Dashboards----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Dashboards", "1")
            self.st.ui_run("Dashboards_search", "1", "test_data_export")
            self.st.ui_run("Dashboards_search", "2")
            self.st.ui_run("Dashboards_search", "3", "test_data_export")
            self.st.ui_run("Dashboards_search", "4")
            time.sleep(2)
            self.st.ui_run("Dashboards_Data", "1")  # 点击数据（Data）
            time.sleep(1)
            print("----verify data search function ----")
            self.st.ui_run("Dashboards_Data", "2_3")
            time.sleep(5)
            self.st.ui_run("Dashboards_Data", "7")
            time.sleep(5)
            self.st.ui_run("Dashboards_Data", "8")
            time.sleep(5)
            self.st.ui_run("Dashboards_Data", "9")
            self.st.ui_run("Dashboards_Data", "10")

        except Exception as e:
            self.add_img()
            assert False, e

    # @unittest.skip("已调试通过，跳过")
    def test_deploy_nlu_with_stopword_list(self):
        try:
            self.st.login_uat()
            # time.sleep(1)
            self.st.ui_run("HomePage_Dashboards", "1")
            print("----create new dashboard-----")
            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Dashboards_New_Dashboard", "0")
            dashboard_nlu_name = "deploy_nlu_stw" + time_now()
            self.st.ui_run("Dashboards_New_Dashboard", "1", dashboard_nlu_name)
            self.st.ui_run("Dashboards_New_Dashboard", "2")
            self.st.ui_run("Dashboards_New_Dashboard", "3")
            time.sleep(3)
            print("----Select a Data Stream----")
            # time.sleep(5)
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "1")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "2")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "3", self.st.lang)
            print("----Deploy NLU Model----")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "1")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_2")
            time.sleep(5)
            # self.st.dashborad_delete_fileds()
            # # time.sleep(5)
            # print("NLU,选择数据-text格式")
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1")
            # # time.sleep(2)
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "5", self.st.lang)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "6")
            # time.sleep(2)
            nlu_name = "nlu_stw" + time_now()
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "6", nlu_name)
            # time.sleep(2)
            print("----编辑高级选项----")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "2_1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "3_1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "4", "DK_stopword_test")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "5")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "7")
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "3")
            time.sleep(5)
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            print("create wiget")
            self.st.ui_run("Dashboards_wiget_add", "1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "2")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "4")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "5")
        except Exception as e:
            self.add_img()
            assert False, e

    # @unittest.skip("已调试通过，跳过")
    def test_deploy_nlu_with_sentiment_list(self):
        try:
            self.st.login_uat()
            # time.sleep(1)
            self.st.ui_run("HomePage_Dashboards", "1")
            print("----create new dashboard-----")
            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Dashboards_New_Dashboard", "0")
            dashboard_tx_name = "deploy_nlu_sen" + time_now()
            self.st.ui_run("Dashboards_New_Dashboard", "1", dashboard_tx_name)
            self.st.ui_run("Dashboards_New_Dashboard", "2")
            self.st.ui_run("Dashboards_New_Dashboard", "3")
            time.sleep(3)
            print("----Select a Data Stream----")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "1")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "2")
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "3", self.st.lang)
            print("----Deploy NLU Model----")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "1")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_2")
            time.sleep(5)
            # self.st.dashborad_delete_fileds()
            # print("NLU,选择数据-text格式")
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1")
            # # time.sleep(2)
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            # self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "5", self.st.lang)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "6")
            # time.sleep(2)
            nlu_name = "nlu_sen" + time_now()
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "6", nlu_name)
            # time.sleep(2)
            print("----编辑高级选项----")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "2_2")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "3_2")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "4", "dk_sentiment_test")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "5")
            # time.sleep(2)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "7")
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "3")
            time.sleep(5)
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)
            print("create wiget")
            self.st.ui_run("Dashboards_wiget_add", "1")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "2")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "4")
            # time.sleep(2)
            self.st.ui_run("Dashboards_wiget_editer", "5",self.st.lang)
        except Exception as e:
            self.add_img()
            assert False, e

    def test_add_dataStreams_and_verify_trigger_reprocess(self):
        try:
            print("----create a dashboard----")
            self.st.login_uat()

            DataStream =self.st.create_dataStreams()
            # time.sleep(1)
            self.st.ui_run("HomePage_Dashboards", "1")

            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Dashboards_New_Dashboard", "0")
            dashboard_tx_name = "deploy_txonomy1T" + time_now()
            self.st.ui_run("Dashboards_New_Dashboard", "1", dashboard_tx_name)
            self.st.ui_run("Dashboards_New_Dashboard", "2")
            self.st.ui_run("Dashboards_New_Dashboard", "3")
            time.sleep(3)
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "1", DataStream)
            self.st.ui_run("Dashboards_Select_a_Data_Stream", "2")
            time.sleep(1)
            # self.st.ui_run("Dashboards_Select_a_Data_Stream", "3", self.st.lang)
            if self.st.lang=="lang-en":
                self.st.ui_run("Dashboards_Select_a_Data_Stream", "3", "1 file(s) ingested, 1,096 records ready for analysis")
            if self.st.lang=="lang-zh":
                self.st.ui_run("Dashboards_Select_a_Data_Stream", "3",
                               "1 文件已完成录入, 1,096条数据可用于分析任务")
            time.sleep(1)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "1")
            time.sleep(5)
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "2_1")
            time.sleep(5)
            print("Taxonomy,选择数据-text格式")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "1")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "3")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "5", self.st.lang)
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Assigne_Fields", "6")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "1")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "2")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "3")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "4")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Select_Taxonomy", "5")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "6")
            self.st.ui_run("Dashboards_Deploy_a_New_Model_Complete_Submit", "7")
            self.st.ui_run("Dashboards_Select_Deploy_Mode", "3")
            time.sleep(2)
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)

            print("----add a datastream----")
            time.sleep(1)
            self.st.ui_run("Dashboards_Overview", "4")
            time.sleep(1)
            self.st.ui_run("Dashboards_Overview", "5")
            time.sleep(5)
            upload = self.st.find_by("css", ".pull-right input")
            csv_file = os.path.join(parent_path, "user_data/sample_data_1/hh_taxonomy.csv")
            self.st.driver.execute_script(
                'arguments[0].style = ""; arguments[0].style.display = "block"; arguments[0].style.visibility = "visible";',
                upload)
            upload.send_keys(csv_file)
            time.sleep(5)
            if self.st.lang=="lang-en":
                self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "4_2", "2 file(s) ingested, 2,192 records ready for analysis")
            if self.st.lang=="lang-zh":
                self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "4_2",
                               "2 文件已完成录入, 2,192条数据可用于分析任务")
            self.st.ui_run("Data_Streams_Connect_to_the_Data_Stream", "5")
            time.sleep(1)
            self.st.web_refresh()
            self.st.overview_open("Data")
            self.st.ui_run("Dashboards_Overview", "2", self.st.lang)
            self.st.ui_run("Dashboards_Overview", "3", self.st.lang)



        except Exception as e:
            self.add_img()
            assert False, e




if __name__ == '__main__':
    suite = unittest.TestSuite()

    suite.addTest(TC_UAT('test_baidu'))


