import unittest
from common.ex_tool import *
from common.config_set import get_var
from stratifydObject.stratifydUI import stratifyd

cur_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.dirname(cur_path)


class TC_UAT(unittest.TestCase):
    st = stratifyd()

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        cls.st.web_quit()

    def add_img(self):
        self.imgs.append(self.st.get_img())
        return True

    def setUp(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        # self.driver = webdriver.Chrome()
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    def tearDown(self):
        self.st.web_refresh()

    # @unittest.skip("已调试通过，跳过")
    def test_sign_in_error(self):
        try:
            self.st.web_open(get_var("env", "uat_url"))
            self.st.backend_subdomain()
            self.st.login("shaoke.deng@stratifyd.cn", "Deng007")
            get_lang = self.st.find_by("css", ".intercom-cheater").get_attribute('class').split()[1]
            self.st.ui_run("login_alert", "1_1", get_lang)  # 验证登录成功
        except Exception as e:
            self.add_img()
            assert False, e


if __name__ == '__main__':
    unittest.main()
