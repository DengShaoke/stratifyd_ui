import unittest
from common.ex_tool import *
from common.config_set import get_var
from stratifydObject.stratifydUI import stratifyd

cur_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.dirname(cur_path)


class TC_UAT(unittest.TestCase):
    st = stratifyd()

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        cls.st.web_quit()

    def add_img(self):
        self.imgs.append(self.st.get_img())
        return True

    def setUp(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        # self.driver = webdriver.Chrome()
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    def tearDown(self):
        self.st.web_refresh()

    # @unittest.skip("已调试通过，跳过")
    def test_upload_stopword(self):
        '''upload stopword list(file) '''
        stopwords_file = os.path.join(parent_path, get_var("env", "stopwords_file"))
        print(stopwords_file)
        self.st.login_uat()
        self.st.ui_run("HomePage_Advanced", "1")
        self.st.ui_run("Advanced_stopwords", "1")
        self.st.ui_run("Advanced_New_Custom_Stopwords_List", "1")
        self.st.ui_run("Advanced_New_Custom_Stopwords_List", "2", self.st.lang)
        # self.st.ui_run("Advanced_New_Custom_Stopwords_List", "3")
        upload = self.st.find_by("css", ".dialog-block > input[type='file']")
        self.st.driver.execute_script(
            'arguments[0].style = ""; arguments[0].style.display = "block"; arguments[0].style.visibility = "visible";',
            upload)
        upload.send_keys(stopwords_file)
        time.sleep(2)
        self.st.ui_run("Advanced_New_Custom_Stopwords_List", "5")
        self.st.ui_run("Advanced_New_Custom_Stopwords_List", "6")
        stopwords_name = "upload_stopwords" + time_now()
        self.st.ui_run("Advanced_New_Custom_Stopwords_List", "7", stopwords_name)
        self.st.ui_run("Advanced_New_Custom_Stopwords_List", "8")
        time.sleep(2)
        self.st.ui_run("Advanced_New_Custom_Stopwords_List", "9", stopwords_name)

    # @unittest.skip("已调试通过，跳过")
    def test_upload_sentiment(self):
        '''upload sentiment list(file) '''
        sentiment_file = os.path.join(parent_path, get_var("env", "sentiment_file"))
        print(sentiment_file)
        self.st.login_uat()
        # time.sleep(3)
        self.st.ui_run("HomePage_Advanced", "1")
        self.st.ui_run("Advanced_sentiment", "2")
        self.st.ui_run("Advanced_New_Custom_Sentiment_List", "1")
        self.st.ui_run("Advanced_New_Custom_Sentiment_List", "2", self.st.lang)
        # self.st.ui_run("Advanced_New_Custom_Sentiment_List", "3")
        upload = self.st.find_by("css", ".form-group>input[type='file']")
        self.st.driver.execute_script(
            'arguments[0].style = ""; arguments[0].style.display = "block"; arguments[0].style.visibility = "visible";',
            upload)
        upload.send_keys(sentiment_file)
        time.sleep(2)
        print("need upload")
        self.st.ui_run("Advanced_New_Custom_Sentiment_List", "5")
        self.st.ui_run("Advanced_New_Custom_Sentiment_List", "6")
        sentiment_name = "upload_sentiment" + time_now()
        self.st.ui_run("Advanced_New_Custom_Sentiment_List", "7", sentiment_name)
        self.st.ui_run("Advanced_New_Custom_Sentiment_List", "8")
        time.sleep(2)
        self.st.ui_run("Advanced_New_Custom_Sentiment_List", "9", sentiment_name)

    # @unittest.skip("已调试通过，跳过")
    def test_upload_taxonomy(self):
        '''upload taxonomies'''
        taxonomy_file = os.path.join(parent_path, get_var("env", "taxonomy_file"))
        print(taxonomy_file)
        self.st.login_uat()
        # time.sleep(3)
        self.st.ui_run("HomePage_Advanced", "1")
        self.st.ui_run("Advanced_taxonomy", "3")
        self.st.ui_run("Advanced_taxonomy_upload", "1")
        upload = self.st.find_by("css", "input[type='file']")
        self.st.driver.execute_script(
            'arguments[0].style = ""; arguments[0].style.display = "block"; arguments[0].style.visibility = "visible";',
            upload)
        upload.send_keys(taxonomy_file)
        time.sleep(2)
        taxonomy_name = "upload_taxonomy" + time_now()
        self.st.ui_run("Advanced_taxonomy_upload", "3", taxonomy_name)
        self.st.ui_run("Advanced_taxonomy_upload", "4")
        self.st.ui_run("Advanced_taxonomy_upload", "5")
        self.st.ui_run("Advanced_taxonomy_upload", "6")
        time.sleep(2)
        self.st.ui_run("Advanced_taxonomy_upload", "7", taxonomy_name)

    # @unittest.skip("已调试通过，跳过")
    def test_create_seletiment(self):
        self.st.login_uat()
        # time.sleep(3)
        self.st.ui_run("HomePage_Advanced", "1")
        self.st.ui_run("Advanced_sentiment", "2")
        self.st.ui_run("Advanced_New_Custom_Sentiment_List", "1")
        self.st.ui_run("Advanced_New_Custom_Sentiment_List", "2", self.st.lang)
        self.st.create_sentiment()
        sentiment_name = "create_sentiment" + time_now()
        self.st.ui_run("Advanced_New_Custom_Sentiment_List", "7", sentiment_name)
        self.st.ui_run("Advanced_New_Custom_Sentiment_List", "8")
        self.st.ui_run("Advanced_New_Custom_Sentiment_List", "9", sentiment_name)

    # @unittest.skip("已调试通过，跳过")
    def test_create_stopword(self):
        self.st.login_uat()
        # time.sleep(3)
        self.st.ui_run("HomePage_Advanced", "1")
        self.st.ui_run("Advanced_stopwords", "1")
        self.st.ui_run("Advanced_New_Custom_Stopwords_List", "1")
        self.st.ui_run("Advanced_New_Custom_Stopwords_List", "2", self.st.lang)
        self.st.create_stopwords()
        sentiment_name = "create_stopwords" + time_now()
        self.st.ui_run("Advanced_New_Custom_Stopwords_List", "7", sentiment_name)
        self.st.ui_run("Advanced_New_Custom_Stopwords_List", "8")
        self.st.ui_run("Advanced_New_Custom_Stopwords_List", "9", sentiment_name)


if __name__ == '__main__':
    unittest.main()
