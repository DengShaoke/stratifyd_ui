import unittest
from common.ex_tool import *
from common.config_set import get_var
from stratifydObject.stratifydUI import stratifyd

cur_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.dirname(cur_path)


class TC_UAT(unittest.TestCase):
    st = stratifyd()

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        cls.st.web_quit()

    def add_img(self):
        self.imgs.append(self.st.get_img())
        return True

    def setUp(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        # self.driver = webdriver.Chrome()
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    def tearDown(self):
        self.st.web_refresh()

    #  @unittest.skip("新建的用户组不能查询，有bug，等bug修改好了，再调整脚本")
    def test_create_user_group(self):
        '''bug:search function : fail'''
        try:
            self.st.login_uat()
            print("----create the user group----")
            self.st.ui_run("HomePage_Groups", "1")
            self.st.ui_run("common_btn_create", "1")
            group_name = "group_" + time_now()
            self.st.ui_run("Groups_New_User_Group", "1", group_name)
            self.st.ui_run("Groups_New_User_Group", "2")
            self.st.ui_run("Groups_New_User_Group", "3", "test")
            time.sleep(3)
            self.st.ui_run("Groups_New_User_Group", "4")
            self.st.ui_run("Groups_New_User_Group", "5")
            self.st.ui_run("Groups_New_User_Group", "6")
            self.st.ui_run("Groups_New_User_Group", "7")

            print("----verify the user permissions is correct ----")
            self.st.ui_run("common_search_input", "1", group_name)
            self.st.ui_run("common_search_verify", "1", group_name)
            self.st.ui_run("common_search_click", "1")
            self.st.ui_run("Groups_edit_User_Group", "1")
        except Exception as e:
            self.add_img()
            assert False, e



if __name__ == '__main__':
    unittest.main()


