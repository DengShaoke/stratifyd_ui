import unittest
from common.ex_tool import *
from common.config_set import get_var
from stratifydObject.stratifydUI import stratifyd

cur_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.dirname(cur_path)


class TC_UAT(unittest.TestCase):
    st = stratifyd()

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        cls.st.web_quit()

    def add_img(self):
        self.imgs.append(self.st.get_img())
        return True

    def setUp(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        # self.driver = webdriver.Chrome()
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    def tearDown(self):
        self.st.web_refresh()

    def test_train_autolearn_model_on_single_text_filed(self):
        try:
            print("----login and click Models----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Models", "1")
            print("----create new models:AutoLearn----")
            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Model_create_a_new_model", "1_1")
            self.st.ui_run("Model_create_a_new_model", "2")
            self.st.ui_run("Model_create_a_new_model", "3")
            self.st.ui_run("Model_create_a_new_model", "4")
            time.sleep(10)
            self.st.ui_run("Model_create_a_new_model", "5")
            time.sleep(2)
            self.st.ui_run("Model_create_a_new_model", "6_1")
            self.st.ui_run("Model_create_a_new_model", "7_1")
            self.st.ui_run("Model_create_a_new_model", "6_2")
            self.st.ui_run("Model_create_a_new_model", "7_2")
            self.st.ui_run("Model_create_a_new_model", "6_3")
            self.st.ui_run("Model_create_a_new_model", "7_3")
            self.st.ui_run("Model_create_a_new_model", "5")
            autolearn_model = "autolearn1_" + time_now()
            self.st.ui_run("Model_create_a_new_model_Complete_Submit", "1", autolearn_model)
            self.st.ui_run("Model_create_a_new_model_Complete_Submit", "2")
            time.sleep(2)
            print("----verify the model training success----")
            self.st.ui_run("Model", "1", self.st.lang)

        except Exception as e:
            self.add_img()
            assert False, e


    def test_train_autolearn_model_on_two_text_filed(self):
        try:
            print("----login and click Models----")
            self.st.login_uat()
            self.st.ui_run("HomePage_Models", "1")
            print("----create new models:AutoLearn----")
            self.st.ui_run("common_btn_create", "1")
            self.st.ui_run("Model_create_a_new_model", "1_1")
            self.st.ui_run("Model_create_a_new_model", "2")
            self.st.ui_run("Model_create_a_new_model", "3")
            self.st.ui_run("Model_create_a_new_model", "4")
            time.sleep(10)
            self.st.ui_run("Model_create_a_new_model", "5")
            time.sleep(2)
            self.st.ui_run("Model_create_a_new_model", "6_1")
            self.st.ui_run("Model_create_a_new_model", "7_1")
            self.st.ui_run("Model_create_a_new_model", "6_2")
            self.st.ui_run("Model_create_a_new_model", "7_2")
            self.st.ui_run("Model_create_a_new_model", "6_3")
            self.st.ui_run("Model_create_a_new_model", "7_3")
            self.st.ui_run("Model_create_a_new_model", "6_4")
            self.st.ui_run("Model_create_a_new_model", "7_3")
            self.st.ui_run("Model_create_a_new_model", "5")
            autolearn_model = "autolearn2_" + time_now()
            self.st.ui_run("Model_create_a_new_model_Complete_Submit", "1", autolearn_model)
            self.st.ui_run("Model_create_a_new_model_Complete_Submit", "2")
            time.sleep(2)
            print("----verify the model training success----")
            self.st.ui_run("Model", "1", self.st.lang)

        except Exception as e:
            self.add_img()
            assert False, e

if __name__ == '__main__':
    unittest.main()
