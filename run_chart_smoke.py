from utx import *
import logging
import os
import unittest

from common.send_mail import send_mail
from common.HTMLTestRunner_Chart import HTMLTestRunner

cur_path = os.path.dirname(os.path.realpath(__file__))

case_path = os.path.join(cur_path, "stratifydTC")
report_path = os.path.join(cur_path, "report/stratifyd_testReport.html")

if __name__ == '__main__':
    # setting.run_case = {Tag.ALL}  # 运行全部测试用例
    setting.run_case = {Tag.SMOKE}  # 只运行SMOKE标记的测试用例
    # setting.run_case = {Tag.SMOKE, Tag.V1_0_0}   # 只运行SMOKE和V1_0_0标记的测试用例

    setting.check_case_doc = False  # 关闭检测是否编写了测试用例描述
    setting.full_case_name = True
    setting.max_case_name_len = 80  # 测试报告内，显示用例名字的最大程度
    setting.show_error_traceback = True  # 执行用例的时候，显示报错信息
    setting.sort_case = True  # 是否按照编写顺序，对用例进行排序
    setting.show_print_in_console = True
    log.set_level(logging.DEBUG)  # 设置utx的log级别

    # log.set_level_to_debug()     # 设置log级别的另外一种方法
    # 运行测试用例并生成报告
    suite = unittest.defaultTestLoader.discover(case_path, pattern='*.py', top_level_dir=None)
    print(suite)
    runner = HTMLTestRunner(
        title="stratifyd_test_report",
        description="stratifyd BJ TestReport",
        stream=open(report_path, "wb"),
        verbosity=2,
        retry=3,
        save_last_try=True)
    runner.run(suite)
    # 生成测试报告后发送邮件
    print("\n","http://0.0.0.0:8000/stratifyd_testReport.html")
    # send_mail("斯图飞腾-自动化测试报告")
